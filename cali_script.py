'''Script for gold pad calibration.'''

import json
import numpy as np
import pickle

from xrd_calibration import get_shift_using_curve_fit
from util import read_image 
from Interpolate2D import interpolate2d

mat_sys_name = '' # pylint: disable=invalid-name
#FILE_PATH = '/nfs/chess/daq/2021-1/id3b/vandover/{}'.format(mat_sys_name)\
#          + '/Calibration/calib_raw/{}-calib_pad_+{}_+{}/*.tiffs'
FILE_PATH = '/home/ming-chiang/Downloads/{}-calib_{}_{}_001_output.npy'
OUT_PATH = '/home/ming-chiang/Downloads/shift.pkl' 
#OUT_PATH = ('/nfs/chess/daq/2021-1/id3b'
#            '/vandover/{}/Calibration/').format(mat_sys_name) # Pseudo
X, Y = (0, 1)
DISTANCE_PER_FRAME = 10 #um

directions = ['h','v']
default_x = [0,0,32,40,32,0,-32,-40,-32]
default_y = [0,40,30,0,-30,-40,-30,0,30]

calib = {}
for x, y in zip(default_x, default_y):
    shift_arr = np.zeros(2)
    for direction in directions:
        # At an abstract level, this line has to load raw data into arrays
        # that contains all images in a linesacn. Modify the readh5 function
        # in util as needed
        images = read_image(FILE_PATH.format(direction, str(x), str(y)))
        shift = get_shift_using_curve_fit(images, plot=False)

        if direction == 'h':
            shift_arr[X] = shift*DISTANCE_PER_FRAME
        if direction == 'v':
            shift_arr[Y] = shift*DISTANCE_PER_FRAME
    calib[(x,y)] = tuple(shift_arr)  # Could use named tuple if necessary

print(calib)

points = [list(key) for key in calib]
x_values = [calib[key][0] for key in calib]
y_values = [calib[key][1] for key in calib]
fpt = [[40,5],[5,7],[-2,3]]
interpolate2d(points, x_values, fpt, plot = True, scaling = 1.2)
interpolate2d(points, y_values, fpt, plot = True, scaling = 1.2)


########## Data storage ############3
# with open(OUT_PATH, 'wb') as fp:
#     pickle.dump(calib, fp)
