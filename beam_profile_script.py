import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import gaussian

from xrd_calibration import get_beam_profile
from util import readh5

fp = '/Users/mingchiang/Desktop/Data/2021-CHESS/'
#intensities=np.load(fp+'search_002.npy')
intensities=np.load(fp+'Iscan_search_001.npy')
directions = ['h','v']
default_x = [0,0,32,40,32,0,-32,-40,-32]
default_y = [0,40,30,0,-30,-40,-30,0,30]
calib_x = []
calib_y = []
distance_per_frame_in_x = 10 #um
distance_per_frame_in_y = 10 #um

_, sigma = get_beam_profile(intensities, plot=True)
print(sigma)
