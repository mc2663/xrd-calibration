'''
This module is built to calibrate xrd alignment. There are 9 metal pads on
a wafer and we will acquire xrd scans along the x and y axis. These function
will help analyze the acquired xrd scans and find the center in each direction.
For usage, please reference the cali_script.py that comes with this module.
'''

import numpy as np
import imageio
import matplotlib.pyplot as plt
from scipy.special import erf
from scipy.optimize import curve_fit
from scipy.signal import gaussian

def get_shift_using_gradient(images, edge_pxls=10, std_threshold=10, plot=False):
    '''
    Get shift by finding the center position from the intensity of images
    and subtract it by the nominal center position
    '''
    intensities = np.array([get_total_intensity(image) 
                            for image in images]).astype('float64')
    grad = get_gradient(intensities)
    calib_center = get_center(grad, edge_pxls, std_threshold, plot=plot)
    center = intensities.shape[0]/2
    if plot:
        plt.plot(intensities)
        plt.scatter(center, intensities[int(center)], c='r')
        plt.show()
    return calib_center-center

def get_shift_using_curve_fit(images, plot=False):
    intensities = np.array([get_total_intensity(image) 
                            for image in images]).astype('float64')
    mid = int(intensities.shape[0]/2)
    miu_left, _ = gauss_cdf_fit(intensities[:mid], plot)
    miu_right, _ = gauss_cdf_fit(intensities[mid:][::-1], plot)
    miu_right = intensities.shape[0]-miu_right
    print(miu_left, miu_right)
    center = (miu_left+miu_right)/2
    return center-mid

def get_gradient(intensities):
    gradient = intensities-np.roll(intensities, 1)
    gradient[0] = get_pseudo(gradient)
    return gradient

def get_pseudo(gradient):
    return np.mean(gradient[1:10])

def get_beam_profile(intensity_profile, edge_pxls=20, std_threshold=5,
                     trans_half_width=30, plot=False):
    '''
    Beam profile is obtained by finding the transition of a linescan
    across a gold pad and trying to fit the transition by a Gaussian
    CDF. A useful formula here is that the FWHM of a Gaussian equals
    to 2.355*sigma.
    '''
    trans = get_transition(intensity_profile, 20, 5)
    scope = (trans-trans_half_width, trans+trans_half_width)
    print('Looking at pixel range {} to {}'.format(scope[0], scope[1]))
    miu, sigma = gauss_cdf_fit(intensity_profile[scope[0]:scope[1]],
                               plot=plot)
    return miu, sigma

def gauss_cdf_fit(data, plot=False):
    '''
    Fit the curve in data using a Gaussion CDF.
    This is useful for finding the beam profile.
    The data can be assuemd to be a convolution between a Gaussian
    and a step function.
    '''
    x = np.arange(data.shape[0])
    data = data/np.max(data)
    def cdf(x, miu, sigma, shift, scale):
        return shift + scale*0.5*(1+erf((x-miu)/(np.sqrt(2)*sigma))) 

    popt, pcov = curve_fit(cdf, x, data) 
    miu = popt[0]
    sigma = popt[1]
    if plot:
        plt.plot(x, data, label='Origin data')
        plt.plot(x, cdf(x, *popt), label='Fitted')
        plt.legend()
        plt.show()
    return miu, sigma

def plot_gaussian(sigma):
    gauss = gaussian(100, sigma)
    plt.plot(gauss)
    plt.show()

def get_total_intensity(image):
    '''
    Summing the intensity of image 
    '''
    return np.sum(np.array(image))

def get_center(intensities, edge_pxls=10, std_threshold=3,
               pxls_to_skip=30, plot=False):
    '''
    Using pixels at the edge to find the baseline and std
    Define the edge of the gold pad by finding the frame where the
    intensity exceeds a multiple of std
    Return the center position of the pad in unit of frames
    '''
    intensities = np.array(intensities)

    first_tran = get_transition(intensities, edge_pxls,
                                std_threshold, plot=plot)
    next_tran = get_transition(intensities[first_tran+pxls_to_skip:],
                               edge_pxls, std_threshold, plot=plot)
    second_tran = first_tran+next_tran+pxls_to_skip
    print(first_tran, second_tran)
    center = (first_tran+second_tran)/2

    if plot:
        plt.plot(intensities)
        plt.scatter(center, intensities[int(center)], c='r')
        plt.show()

    return center 

def get_transition(intensities, edge_pxls, std_threshold, plot=False):
    '''
    Calculate baseline and std and find the transition where the
    value > baseline+std*std_threshold
    edge_pxls parameter can be used to control the range that is
    count as edge, which is used to calculate baseline and std
    '''
    baseline = get_baseline_at_edges(intensities, edge_pxls)
    std = get_std_at_edges(intensities, edge_pxls)

    for idx, intensity in enumerate(intensities):
        if intensity > baseline+std*std_threshold or\
           intensity < baseline-std*std_threshold:
            if plot:
                plt.plot(intensities)
                plt.scatter(idx, intensities[idx], c='r')
                plt.show()
            return idx
    return -1 #psuedo

def get_baseline_at_edges(intensities, pxls=10):
    '''
    mean of the edge pixels as baseline
    '''
    return np.mean(intensities[1:1+pxls])

def get_std_at_edges(intensities, pxls=10):
    '''
    std of edge pixels
    '''
    return np.std(intensities[1:1+pxls])


