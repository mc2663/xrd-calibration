import glob
from xrd_alignment import get_total_intensity, get_center
import matplotlib.pyplot as plt

mat_sys_name = ''
re = '/Users/mingchiang/Desktop/Au_align/scan_002/*.tiff'
directions = ['h','v']
default_x = [0,0,32,40,32,0,-32,-40,-32]
default_y = [0,40,30,0,-30,-40,-30,0,30]
distance_per_frame = 10 # in um

centers = {}

for x, y in zip(default_x, default_y):
    tiffs = sorted(glob.glob(re))

    intensities = [get_total_intensity(tiff) for tiff in tiffs]
    center = get_center(intensities)
    plt.plot(intensities)
    plt.scatter(center, intensities[int(center)], c='r')
    plt.show()
    #center = get_center(intensities)

    #centers[(x,y)][direction] = cente
