import numpy as np

def read_image(path):
    '''
    Specifically for 2021 Feb CHESS run file structure
    '''
    return np.load(path)

