import pickle
import numpy as np
import copy as cp
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation, LinearTriInterpolator, CubicTriInterpolator
from scipy.spatial import ConvexHull


def interpolate2d(points_in, values_in, fpt, plot = False, method = "linear", scaling = None):
    """
    points are the list of n xy coordinates of a mesh,
    values is a list of length n with the data to interpolate,
    and fpt is (a list of) the position at which to evaluate the function
    Interpolation is "linear" or cubic with "min_E" or "geom"
    Add some scaling value to allow for extrapolation based on the values on
    the convex hull
    """

    points = np.array(cp.deepcopy(points_in))
    values = np.array(cp.deepcopy(values_in))

    if scaling is not None:
        cm = np.sum(points, axis=0)/points.shape[0]
        hull = ConvexHull(np.array(points))
        if plot:
            plt.plot(points[hull.vertices,0], points[hull.vertices,1], 'r--', lw=2)
            plt.plot(points[hull.vertices[0],0], points[hull.vertices[0],1], 'ro')
            plt.show()
        hp = points[hull.vertices, :]
        hp[:, 0] = hp[:, 0] - cm[0]
        hp[:, 1] = hp[:, 1] - cm[1]
        print(hp)
        hp = hp* scaling
        hp[:, 0] = hp[:, 0] + cm[0]
        hp[:, 1] = hp[:, 1] + cm[1]
        points = np.concatenate((points, hp), axis=0)
        values = np.concatenate((values, values[hull.vertices]), axis=0)


    Xv =  np.array(points)[:,0]
    Yv =  np.array(points)[:,1]
    triObj = Triangulation(Xv,Yv) 
    Zv = values

    
    pt = np.array(fpt)
    if pt.ndim == 1:
        ptx = pt[0]
        pty = pt[1]
    else:
        ptx = pt[:,0]
        pty = pt[:,1]

    r = None
    if method == "linear" or plot:
        #linear interpolation
        fzl = LinearTriInterpolator(triObj,Zv)
        Zl = fzl(ptx, pty)
        if method == "linear":
            r = Zl
        if plot:
            print(Zl)

    if method == "geom" or plot:
        #cubic interpolation geom
        fzcg = CubicTriInterpolator(triObj,Zv, kind = 'geom')
        Zcg = fzcg(ptx,pty)
        if method == "geom":
            r = Zcg
        if plot:
            print(Zcg)

    if method == "min_E" or plot:
        #cubic interpolation min_E
        fzcm = CubicTriInterpolator(triObj,Zv, kind = 'min_E')
        Zcm = fzcm(ptx,pty)
        if method == "min_E":
            r = Zcm
        if plot:
            print(Zcm)

    if r is None:
        print("Interpolation method not implemented")

    if plot:
        # Interpolate to regularly-spaced quad grid.
        xi, yi = np.meshgrid(np.linspace(np.min(Xv), np.max(Xv), 50),
                             np.linspace(np.min(Yv), np.max(Yv), 50))
        zi_lin = fzl(xi, yi)
        zi_cubic_geom = fzcg(xi, yi)
        zi_cubic_min_E = fzcg(xi, yi)
        
        # Set up the figure
        fig, axs = plt.subplots(nrows=2, ncols=2)
        axs = axs.flatten()
        
        # Plot the triangulation.
        triangular = axs[0].tricontourf(triObj, Zv)
        axs[0].triplot(triObj, 'ko-')
        axs[0].set_title('Triangular grid')
        fig.colorbar(triangular, ax=axs[0])

        # Plot linear interpolation to quad grid.
        linear = axs[1].contourf(xi, yi, zi_lin)
        axs[1].plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
        axs[1].plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
        axs[1].set_title("Linear interpolation")
        fig.colorbar(linear, ax=axs[1])

        # Plot cubic interpolation to quad grid, kind=geom
        geom = axs[2].contourf(xi, yi, zi_cubic_geom)
        axs[2].plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
        axs[2].plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
        axs[2].set_title("Cubic interpolation,\nkind='geom'")
        fig.colorbar(geom, ax=axs[2])

        # Plot cubic interpolation to quad grid, kind=min_E
        min_E = axs[3].contourf(xi, yi, zi_cubic_min_E)
        axs[3].plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
        axs[3].plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
        axs[3].set_title("Cubic interpolation,\nkind='min_E'")
        fig.colorbar(min_E, ax=axs[3])
        
        fig.tight_layout()
        plt.show()

    return r


if __name__ == "__main__":
    #points = []
    #npoints = 0
    with open("/home/ming-chiang/Downloads/shift.pkl", 'rb') as fp:
        shift = pickle.load(fp) 
    print(shift)
    #values = range(len(points))
    points = [list(key) for key in shift]
    x_values = [shift[key][0] for key in shift]
    y_values = [shift[key][1] for key in shift]
    print(points, x_values)

    fpt = [[40,5],[5,7],[-2,3]]
    data = interpolate2d(points, x_values, fpt, plot = True, scaling = 1.2)
    data = interpolate2d(points, x_values, fpt, method = "min_E", scaling = 1.2)
    data = interpolate2d(points, y_values, fpt, plot = True, scaling = 1.2)
    data = interpolate2d(points, y_values, fpt, method = "min_E", scaling = 1.2)
    print(data)
    
    #points = []
    #npoints = 0
    #with open("mesh_25.txt") as fp:
    #    Lines = fp.readlines()
    #    for line in Lines:
    #        npoints += 1
    #        points.append([float(i) for i in line.strip().split()])
    #    print(points)
    #values = range(len(points))

    #fpt = [[40,5],[5,7],[-2,3]]
    #data = interpolate2d(points, values, fpt, plot = True, scaling = 1.2)
    #data = interpolate2d(points, values, fpt, method = "min_E", scaling = 1.2)
    #print(data)



